const numberOfFilms = prompt("Сколько фильмов вы уже посмотрели?");

const personalMovieDB = {
  count: numberOfFilms,
  movies: {},
  actors: {},
  genres: [],
  privat: false,
};

const movie1 = prompt("Один из последних просмотренных фильмов?");
const movieScore1 = prompt("На сколько оцените его?");
const movie2 = prompt("Один из последних просмотренных фильмов?");
const movieScore2 = prompt("На сколько оцените его?");

// personalMovieDB.movies[movie1] = movieScore1;
// personalMovieDB.movies[movie2] = movieScore2;

personalMovieDB.movies = {
  ...personalMovieDB.movies,
  movie1: movieScore1,
  movie2: movieScore2,
};

console.log(personalMovieDB);
